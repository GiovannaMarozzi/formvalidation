import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

export default function Home() {

    const [users, setUsers] = useState([]);

    const {cpfUser}=useParams();

    useEffect(() => {
        loadUsers();
    }, []);

    const loadUsers = async () => {
        const result = await axios.get("http://localhost:8080/register/registerAll");
        setUsers(result.data);
      }

    const deleteUser=async(cpf)=>{
        await axios.delete(`http://localhost:8080/register/cpf=${cpf}`)
        alert("Usuário deletado com sucesso!")
        loadUsers();
    }

    return (
        <div className="container">
            <div className="py-4">
                <table className="table shadow">
                    <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Nome</th>
                            <th scope="col">CPF</th>
                            <th scope="col">RG</th>
                            <th scope="col">Data De Nascimento</th>
                            <th scope="col">Data De Registro</th>
                            <th scope="col">Opções</th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            users.map((user, index) => (
                                <tr>
                                    <th scope="row" key={index}>{index+1}</th>
                                    <td>{user.name}</td>
                                    <td>{user.cpf}</td>
                                    <td>{user.rg}</td>
                                    <td>{user.date_of_birth}</td>
                                    <td>{user.date_register}</td>
                                    <td>
                                        <Link className="btn btn-primary mx-2" to={`/viewuser/${user.cpf}`}>Vizualizar</Link>
                                        <Link className="btn btn-outline-primary mx-2" to={`/edituser/${user.cpf}`} state={{ cpfUser: user.cpf }}>Editar</Link>
                                        <button className="btn btn-danger mx-2" onClick={()=>deleteUser(user.cpf)}>Excluir</button>
                                    </td>
                                </tr>
                            ))
                        }

                    </tbody>
                </table>
            </div>
        </div>
    )
}