import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useParams } from "react-router-dom";

export default function ViewUser() {
  const [user, setUser] = useState(null);
  const { cpf } = useParams();

  useEffect(() => {
    loadUser();
  }, []);

  const loadUser = async () => {
    try {
      const response = await axios.get(`http://localhost:8080/register/cpf=${cpf}`);
      setUser(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <div className="container">
      <div className="row gx-5">
        <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
          {user ? (
            <div className="card-body">
              <h5 className="card-title">CPF: {user?.cpf}</h5>
              <ul className="list-group list-group-flush">
                <li className="list-group-item">
                  <b>Nome:</b> {user?.name}
                </li>
                <li className="list-group-item">
                  <b>RG:</b> {user?.rg}
                </li>
                <li className="list-group-item">
                  <b>Data de Nascimento:</b> {user?.date_of_birth}
                </li>
                <li className="list-group-item">
                  <b>Data de Registro:</b> {user?.date_register}
                </li>
                <li className="list-group-item">
                  <b>Email:</b> {user?.contacts?.email}
                </li>
                <li className="list-group-item">
                  <b>Telefone:</b> {user?.contacts?.phone}
                </li>
                <li className="list-group-item">
                  <b>Endereço:</b> {user?.address?.publicPlace}, {user?.address?.homeNumber}, {user?.address?.neighborhood}, {user?.address?.complement}, CEP: {user?.address?.cep}
                </li>
              </ul>
            </div>
          ) : (
            <div>Loading...</div>
          )}
          <Link className="btn btn-primary my-2" to="/">Home</Link>
        </div>
      </div>
    </div>
  );
}
