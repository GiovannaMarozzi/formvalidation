import React, { useEffect, useState } from "react";
import axios from "axios";
import { Link, useNavigate, useParams, useLocation } from "react-router-dom";

export default function EditUser() {
    let navigate = useNavigate();

    const location = useLocation();
    const { cpfUser } = location.state || {};

    const [user, setUser] = useState({
        name: "",
        cpf: "",
        rg: "",
        address: {
            publicPlace: "",
            homeNumber: "",
            neighborhood: "",
            complement: "",
            cep: ""
        },
        date_of_birth: "",
        date_register: "",
        contacts: {
            email: "",
            phone: ""
        }
    });

    const { name, cpf, rg, address, date_of_birth, date_register, contacts } = user;

    const handleChange = (e) => {
        const { name, value } = e.target;
        if (name.includes(".")) {
            const [parentField, childField] = name.split(".");
            setUser((prevState) => ({
                ...prevState,
                [parentField]: {
                    ...prevState[parentField],
                    [childField]: value
                }
            }));
        } else {
            setUser((prevState) => ({
                ...prevState,
                [name]: value
            }));
        }
    };

    useEffect(() => {
        loadUser();
    }, []);

    const handleSubmit = async (e) => {
        e.preventDefault();

        try {
            await axios.put(`http://localhost:8080/register/cpf=${cpf}`, {
                ...user,
                address: { ...address },
                contacts: { ...contacts }
            });

            navigate("/");
            console.log("Dados enviados com sucesso!");
        } catch (error) {
            console.error(error);
        }
    };

    const loadUser = async () => {
        const result = await axios.get(`http://localhost:8080/register/cpf=${cpfUser}`);
        setUser(result.data)
    }

    return (
        <div className="container">
            <div className="row gx-5">
                <div className="col-md-6 offset-md-3 border rounded p-4 mt-2 shadow">
                    <h3 className="text-center">Editar Usuário</h3>
                    <h5 className="text-center">Dados Pessoais</h5>
                    <form onSubmit={handleSubmit}>
                        <div className="mb-3">
                            <label htmlFor="name" className="form-label">Nome</label>
                            <input type="text" className="form-control" id="name" placeholder="Digite o nome" name="name" value={name} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="cpf" className="form-label">CPF</label>
                            <input type="text" className="form-control" id="cpf" placeholder="Digite o CPF" name="cpf" value={cpf} onChange={handleChange} disabled/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="rg" className="form-label">RG</label>
                            <input type="text" className="form-control" id="rg" placeholder="Digite o RG" name="rg" value={rg} onChange={handleChange} disabled/>
                        </div>
                        <div className="mb-3">
                            <label htmlFor="date_of_birth" className="form-label">Data de Nascimento</label>
                            <input type="text" className="form-control" id="date_of_birth" placeholder="Digite a data de nascimento" name="date_of_birth" value={date_of_birth} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="date_register" className="form-label">Data de Registro</label>
                            <input type="text" className="form-control" id="date_register" placeholder="Digite a data de registro" name="date_register" value={date_register} onChange={handleChange} dis/>
                        </div>
                        <br />

                        <h5 className="text-center">Contatos</h5>
                        <div className="mb-3">
                            <label htmlFor="email" className="form-label">Email</label>
                            <input type="text" className="form-control" id="email" placeholder="Digite o email" name="contacts.email" value={contacts.email} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="phone" className="form-label">Telefone</label>
                            <input type="text" className="form-control" id="phone" placeholder="Digite o telefone" name="contacts.phone" value={contacts.phone} onChange={handleChange} />
                        </div>
                        <br />

                        <h5 className="text-center">Endereço</h5>
                        <div className="mb-3">
                            <label htmlFor="publicPlace" className="form-label">Logradouro</label>
                            <input type="text" className="form-control" id="publicPlace" placeholder="Digite o logradouro" name="address.publicPlace" value={address.publicPlace} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="homeNumber" className="form-label">Número</label>
                            <input type="text" className="form-control" id="homeNumber" placeholder="Digite o número" name="address.homeNumber" value={address.homeNumber} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="neighborhood" className="form-label">Bairro</label>
                            <input type="text" className="form-control" id="neighborhood" placeholder="Digite o bairro" name="address.neighborhood" value={address.neighborhood} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="complement" className="form-label">Complemento</label>
                            <input type="text" className="form-control" id="complement" placeholder="Digite o complemento" name="address.complement" value={address.complement} onChange={handleChange} />
                        </div>
                        <div className="mb-3">
                            <label htmlFor="cep" className="form-label">CEP</label>
                            <input type="text" className="form-control" id="cep" placeholder="Digite o CEP" name="address.cep" value={address.cep} onChange={handleChange} />
                        </div>
                        <button type="submit" className="btn btn-outline-primary">Enviar</button>
                        <Link className="btn btn-outline-danger mx-2" to="/">Cancelar</Link>
                    </form>
                </div>
            </div>
        </div>
    );
}
