**Form Validation**

This repository contains the source code for the Person Registration and Validation Form using React. The main goal is to enable CRUD (Create, Read, Update, Delete) operations for registering people.

Upon accessing the application, a table will be displayed with all the individuals registered in the database. Additionally, there is a button to register new people. Clicking on this button will redirect you to a registration screen with the necessary input fields according to the required JSON format. After completing the form, you can click the "Submit" button to finalize the registration or the "Cancel" button to abort it.

In the table, each row features buttons to update, view, and delete data. The update option allows modifying the information, except for the CPF, RG, and Registration Date. The view option displays all the corresponding personal, contact, and address details for the selected user.

Deletion works based on the CPF. By clicking on the corresponding button, the respective CPF will be retrieved, and a deletion request will be sent directly to the backend.

To get the project up and running, follow these steps:
1. Run the command "npm start" in the terminal.
2. Access the address http://localhost:3000 in your browser.

Note: This project should be used in conjunction with the _ValidationPerson_ project.
